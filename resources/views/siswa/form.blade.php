@extends('layouts.app')
@section('content')

    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <a href="{{ url('/siswa') }}" class="btn bg-purple"><i class="fa fa-chevron-left"></i> Kembali</a>
            </div>
            <div class="box-body">
                
            <form action="{{ !empty($data) ? url('/siswa/' . $data->nis . '/update') : url('/siswa/add') }}" method="POST" class="form-horizontal" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group">
                    <label class="control-label col-sm-2">NIS</label>
                    <div class="col-sm-9">
                        <input type="text" name="nis" class="form-control" placeholder="Masukkan NIS" value="{{ empty($data) ? old('nis') : $data->nis }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Nama Lengkap</label>
                    <div class="col-sm-9">
                        <input type="text" name="nama_lengkap" class="form-control" placeholder="Masukkan Nama Lengkap" value="{{ empty($data) ? old('nama_lengkap') : $data->nama_lengkap }}">
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-sm-2">Jenis Kelamin</label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label><input type="radio" name="jenis_kelamin" value="L" {{ empty($data) ? '' : ($data->jenis_Kelamin == 'Laki - Laki') ? 'checked' : '' }}> L</label>
                            <label><input type="radio" name="jenis_kelamin" value="P" {{ empty($data) ? '' : ($data->jenis_Kelamin == 'Perempuan') ? 'checked' : '' }}> P</label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Alamat</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="alamat" style="resize: none;" placeholder="Masukkan Alamat">{{ empty($data) ? old('alamat') : $data->alamat }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">No Telp</label>
                    <div class="col-sm-9">
                        <input type="text" name="no_telp" class="form-control" placeholder="Masukkan No Telp" value="{{ empty($data) ? old('no_telp') : $data->no_telp }}">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Kelas</label>
                    <div class="col-sm-9">
                        <select name="id_kelas" class="form-control">
                            <option value="">== Pilih Kelas ==</option>
                            @foreach (\App\Kelas::all() as $val)
                            <option value="{{$val->id_kelas}}" {{ empty($data) ? '' : ($data->id_kelas == $val->id_kelas) ? 'selected' : '' }}>{{ $val->nama_kelas }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-sm-2">Foto</label>
                    <div class="col-sm-9">
                        <input type="file" name="foto" placeholder="Masukkan Foto">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-2">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                    </div>
                </div>
            </form>

            </div>
        </div>

     </section>

@endsection