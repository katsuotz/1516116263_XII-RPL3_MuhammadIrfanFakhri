@extends('layouts.app')
@section('content')

	<section class="content-header">
      <h1>
        Data Siswa
        <small>SMK Negeri 4 Bandung</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Data Siswa</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    	@include('layouts.feedback')
    	<div class="box">
    		<div class="box-header with-border">
    			<a href="{{ url('siswa/add') }}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> Tambah</a>
    		</div>
    		<div class="box-body">
    			
			    <table class="table table-stripped">
			    	<thead>
			    		<tr>
			    			<th>No</th>
			    			<th>Foto</th>
			    			<th>Nama Lengkap</th>
			    			<th>Jenis Kelamin</th>
			    			<th>Alamat</th>
			    			<th>No Telp</th>
			    			<th>Kelas</th>
			    			<th>Action</th>
			    		</tr>
			    	</thead>
			    	<tbody>
			    		@foreach ($siswa as $val)
			    		<tr>
			    			<td>{{ (!empty($i)) ? ++$i : $i = 1 }}</td>
			    			<td><img src="{{ url('uploads/' . $val->foto) }}" width="50" height="50"></td>
			    			<td>{{ $val->nama_lengkap }}</td>
			    			<td>{{ $val->jenis_kelamin }}</td>
			    			<td>{{ $val->alamat }}</td>
			    			<td>{{ $val->no_telp }}</td>
			    			<td>{{ $val->kelas->nama_kelas }}</td>
			    			<td>
			    				<a href="{{ url('siswa/' . $val->nis . '/edit') }}" class="btn btn-sm btn-warning"><i class="fa fa-pencil"></i></a>
			    				<form action="{{ url('siswa/' . $val->nis . '/delete') }}" style="display: inline;">
			    					{{ csrf_field() }}
				    				<button class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button>
			    				</form>
			    			</td>
			    		</tr>
			    		@endforeach
			    	</tbody>
			    </table>

    		</div>
    	</div>

	 </section>

@endsection