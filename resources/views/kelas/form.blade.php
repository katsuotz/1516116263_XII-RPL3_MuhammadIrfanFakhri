@extends('layouts.app')
@section('content')

	<section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    	<div class="box">
    		<div class="box-header with-border">
    			<a href="{{ url('/') }}" class="btn bg-purple"><i class="fa fa-chevron-left"></i> Kembali</a>
    		</div>
    		<div class="box-body">
    			
    		<form action="{{ !empty($data) ? url('/kelas/' . $data->id_kelas . '/update') : url('/kelas/add') }}" method="POST" class="form-horizontal">
    			{{ csrf_field() }}
    			<div class="form-group">
    				<label class="control-label col-sm-2">Nama Kelas</label>
    				<div class="col-sm-9">
    					<input type="text" name="nama_kelas" class="form-control" placeholder="Masukkan Nama Kelas" value="{{ empty($data) ? old('nama_kelas') : $data->nama_kelas }}">
    				</div>
    			</div>

    			<div class="form-group">
    				<label class="control-label col-sm-2">Jurusan</label>
    				<div class="col-sm-9">
    					<input type="text" name="jurusan" class="form-control" placeholder="Masukkan Jurusan" value="{{ empty($data) ? old('jurusan') : $data->jurusan }}">
    				</div>
    			</div>

    			<div class="form-group">
    				<div class="col-sm-10 col-sm-offset-2">
    					<button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
    				</div>
    			</div>
    		</form>

    		</div>
    	</div>

	 </section>

@endsection