<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	return view('siswa.index', [
    		'siswa' => \App\Siswa::all(),
    		]);
    }

    public function create()
    {
    	return view('siswa.form');
    }

    public function store(Request $request)
    {
    	$this->validate($request, $this->rules());
    	$input = $request->all();

        if ($request->hasFile('foto') && $request->file('foto')->isValid()) {
            $input['foto'] = $input['nis'] . '.' . $input['foto']->getClientOriginalExtension();
            $request->file('foto')->storeAs('', $input['foto']);
        }

    	if (\App\Siswa::create($input))
    		return redirect('/siswa')->with('success', 'Data berhasil ditambahkan');
		return redirect('/siswa')->with('error', 'Data gagal ditambahkan');
    }

    public function edit($id='')
    {
    	return view('siswa.form', [
    		'data' => \App\Siswa::where('nis', $id)->first(),
    		]);
    }

    public function update($id='', Request $request)
    {
    	$this->validate($request, $this->rules());
    	$input = $request->except('_token');
    	if (\App\Siswa::where('nis', $id)->update($input))
    		return redirect('/siswa')->with('success', 'Data berhasil diubah');
		return redirect('/siswa')->with('error', 'Data gagal diubah');
    }

    public function destroy($id='')
    {
    	if (\App\Siswa::where('nis', $id)->delete())
    		return redirect('/siswa')->with('success', 'Data berhasil dihapus');
		return redirect('/siswa')->with('error', 'Data gagal dihapus');
    	
    }

    public function rules()
    {
    	return [
    		'nis' => 'required',
    		'nama_lengkap' => 'required|max:100',
    		'jenis_kelamin' => 'required',
    		'alamat' => 'required',
    		'no_telp' => 'required',
    		'id_kelas' => 'required|exists:t_kelas',
            'foto' => 'required|mimes:jpeg,png|max:512',
    	];
    }
}
